# mavlink-router

Dockerized mavlink-router (https://github.com/mavlink-router/mavlink-router)

## Build
```
sudo docker build -t fhgrderungs/mavlink-router .
```

## Publish
```
sudo docker push fhgrderungs/mavlink-router
```
