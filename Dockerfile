FROM ubuntu:22.04

# EXAMPLE Dockerfile with Ubuntu.
# This is the lightweight version, it use a two stages build. First stage will download the development dependencies
# and compile mavlink-router, the second stage will get the binary from the first stage and use it.
# From mavlink-router root directory, run `docker build . -t mavlink-router -f examples/Dockerfile` to build
# then `docker run --rm -it --network=host mavlink-router` can be used to run, adapt on your need.

#ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir /mavlink-router

ADD https://github.com/mavlink-router/mavlink-router/releases/download/v3/mavlink-routerd-glibc-x86_64 /mavlink-router

RUN chmod +x /mavlink-router/mavlink-routerd-glibc-x86_64

# -t 0 -> Disable default TCP Client
ENTRYPOINT ["/mavlink-router/mavlink-routerd-glibc-x86_64", "-t", "0"]
